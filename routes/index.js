var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  

  var HOST_URL;
  if (process.env.HOST != null){
    HOST_URL = process.env.HOST;
  } else {
    HOST_URL = "http://localhost:3000";
  }

  res.render('index', 
  { title: 'Express',
    host: HOST_URL  
  });
});

module.exports = router;
