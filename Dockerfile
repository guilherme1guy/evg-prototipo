FROM node:alpine
ADD . /node
WORKDIR /node
CMD ["npm start"]